.. title:: Tornado Web Server

.. meta::
    :google-site-verification: g4bVhgwbVO1d9apCUsT-eKlApg31Cygbp8VGZY8Rf0g

|Tornado Web Server|
====================

.. |Tornado Web Server| image:: tornado.png
    :alt: Tornado Web Server

`Tornado <http://www.tornadoweb.org>`_ 是一个Python的Web框架和异步网络库，是由
`FriendFeed
<http://friendfeed.com>`_
开发的。
得益于无阻塞网络输入输出的使用，Tornado可以实现数以万计的活动连接的规模，适合于
`长轮巡 long polling <http://en.wikipedia.org/wiki/Push_technology#Long_polling>`_,
`WebSockets <http://en.wikipedia.org/wiki/WebSocket>`_，
其他程序则需要保持每一个用户的long-lived链接。


快捷链接
-----------

* :doc:`文档 <documentation>`
* |Download current version|: :current_tarball:`z` (:doc:`发布说明 <releases>`)
* `源码 (github) <https://github.com/facebook/tornado>`_
* `邮件列表 <http://groups.google.com/group/python-tornado>`_
* `Wiki <https://github.com/facebook/tornado/wiki/Links>`_

.. |Download current version| replace:: 下载 |version|

Hello, world
------------
下面是一个简单的Tornado “Hello World” 示例：
::

    import tornado.ioloop
    import tornado.web

    class MainHandler(tornado.web.RequestHandler):
        def get(self):
            self.write("Hello, world")

    application = tornado.web.Application([
        (r"/", MainHandler),
    ])

    if __name__ == "__main__":
        application.listen(8888)
        tornado.ioloop.IOLoop.instance().start()

这个示例没有用到Tornado任何的异步特性；可以查看
`simple chat room
<https://github.com/facebook/tornado/tree/master/demos/chat>`_.

安装
------------

**自动安装**::

    pip install tornado

Tornado 已经发布到 `PyPI <http://pypi.python.org/pypi/tornado>`_ 所以可以通过
``pip`` or ``easy_install`` 进行安装。

请注意，源码发布版本包括了示例程序，使用以上方式安装则不包含，
所以，出于学习目的，最好直接下载源码发布版本。

**手动安装**: 下载 :current_tarball:`z`:

.. parsed-literal::

    tar xvzf tornado-|version|.tar.gz
    cd tornado-|version|
    python setup.py build
    sudo python setup.py install

Tornado的源码托管于 `GitHub
<https://github.com/facebook/tornado>`_.

**先决条件**: Tornado 运行于 Python 2.6, 2.7, 3.2, 和 3.3 版本.
除了Python的标准库以后，没有其他严格的要求；但是一些特性需要以下的几个库：

* 需要 `unittest2 <https://pypi.python.org/pypi/unittest2>`_ 来运行Tornado的测试(Python2.6)
  在新版本的Python中已经不需要了。
* `concurrent.futures <https://pypi.python.org/pypi/futures>`_ 在使用
  `~tornado.netutil.ThreadedResolver` 时，它是推荐的线程池。
  仅仅在Python2版本中需要，Python3已经将其包含在了标准库中。
* `pycurl <http://pycurl.sourceforge.net>`_ 用于
  ``tornado.curl_httpclient``.
  需求Libcurl 7.18.2 以上版本
  推荐Libcurl 7.21.1 版本
* 需求 `Twisted <http://www.twistedmatrix.com>`_ 如果需要使用
  `tornado.platform.twisted`.
* `pycares <https://pypi.python.org/pypi/pycares>`_ 是一个可选的非阻塞DNS解析器
* `Monotime <https://pypi.python.org/pypi/Monotime>`_ adds support for
  a monotonic clock, which improves reliability in environments
  where clock adjustments are frequent.  No longer needed in Python 3.3.

**平台**: Tornado需要运行于类Unix平台，推荐Linux平台( ``epoll`` )或BSD ( ``kqueue`` )
(虽然 Mac OS X 是BSD的衍生版本，而且它也支持kqueue，但是它的网络性能太差，仅仅推荐开发时使用。)

讨论及支持
----------------------

谷歌讨论组 `the Tornado developer mailing list
<http://groups.google.com/group/python-tornado>`_，
BUG提交 `GitHub issue tracker
<https://github.com/facebook/tornado/issues>`_，
资源链接
`Tornado wiki
<https://github.com/facebook/tornado/wiki/Links>`_。

Tornado 是一个 `Facebook 的开源技术
<http://developers.facebook.com/opensource/>`_。
它的发布遵循
`Apache License, Version 2.0
<http://www.apache.org/licenses/LICENSE-2.0.html>`_ 协议。

.. toctree::
   :hidden:

   documentation
