核心Web框架
==================

.. toctree::
   :maxdepth: 2

   web
   httpserver
   template
   escape
   locale
